<!DOCTYPE html>
<html>
<head>
	<title>Index</title>
	<link rel="stylesheet" type="text/css" href="styles.css">
</head>
<body>
	<h1 class="header-h1">WEBDESIGN</h1>
	<div class="header-div">
		<h1>Your text here</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
	</div>
	<div class="body-div2">
		<h1 class="black-h1">HOW DOES IT WORK</h1>
		<h2>Step one</h2>
		<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum.</p>
		<h2>Step two</h2>
		<p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae.</p>
		<h2>Step three</h2>
		<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
		<h1 class="orange-text">WELL DONE!</h1>
	</div>
	<div class="body-div2img"></div>
	<div class="body-div2add"></div>
	<div class="body-div3add"></div>
	<div class="body-div3img"></div>
	<div class="body-div3">
		<h1 class="black-h1">HOW DOES IT WORK</h1>
		<h2>Step one</h2>
		<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum.</p>
		<h2>Step two</h2>
		<p>Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae.</p>
		<h2>Step three</h2>
		<p>Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>
		<form action="buy.php"><input type="submit" value="BUY"></form>
	</div>
	<h1 class="bottom-h1">HOPE TO SEE YOU SOON!</h1>
	<div class="call">
		<h1>YOU CAN ALWAYS CALL</h1>
		<a href="0616997576">PHONE NUMBER</a>
	</div>
	<div class="vert-bar"></div>
</body>
</html>